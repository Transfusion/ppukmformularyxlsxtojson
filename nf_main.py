import re, json, datetime
from nf_word_filter import NFWordFilter


if __name__ == '__main__':
    nf_word_filter = NFWordFilter('./Update NF booklet 2.docx')

    formulary_output = {
        'timestamp': datetime.datetime.now().isoformat(),
        'nf_drugs': nf_word_filter.get_drugs()
    }

    with open('ppukm_nf_test.json', 'w') as outfile:
        json.dump(formulary_output, outfile)
