import re, json, datetime
from excel_filter import ExcelFilter


if __name__ == '__main__':
    excel_filter = ExcelFilter('test.xlsx')
    # print(excel_filter.get_available_fields())
    # print(excel_filter.get_available_categories())
    # print(excel_filter.get_drugs())

    formulary_output = {
        'timestamp': datetime.datetime.now().isoformat(),
        'available_fields': excel_filter.get_available_fields(),
        'available_categories': excel_filter.get_available_categories(),
        'drugs': excel_filter.get_drugs()
    }

    with open('ppukm_formulary.json', 'w') as outfile:
        json.dump(formulary_output, outfile)