import re
from openpyxl import Workbook, load_workbook
import utils


class ExcelFilter:

    def __init__(self, excel_path):
        self.excel_path = excel_path
        self.workbook = load_workbook(excel_path)
        # print(self.workbook.sheetnames)

    def get_available_fields(self):
        data_ws = self.workbook['Sheet1']
        cell_range = data_ws['B1':'G1']
        return [{
                    'id': utils._format_field_name_underscore(utils._remove_html_tags(cell.value)),
                    'name': utils._remove_html_tags(cell.value),
                    'col': cell.column
                }
                for cell in list(cell_range[0])]

    def get_available_categories(self):
        data_ws = self.workbook['Sheet2']
        cell_range = data_ws['I']
        return [{
                    'id': cell.value.replace('Chapter ', '')[0],
                    'name': cell.value.replace('Chapter ', '')[2:],
                    'color': cell.fill.start_color.index
                }
                for cell in cell_range if cell.value is not None]

    def get_drugs(self):
        categories = self.get_available_categories()
        fields = self.get_available_fields()
        data_ws = self.workbook['Sheet1']
        drugs = []

        # return [row for row in data_ws.iter_rows(row_offset=2, min_col=2, max_col=7)]
        row_offset = 2
        cur_row = row_offset

        for row in data_ws.iter_rows(row_offset=row_offset, min_col=1, max_col=7):
            row = list(row)
            row_is_empty = all(cell.value is None for cell in row)
            if row_is_empty:
                break

            drug = {'id': cur_row}
            cur_row += 1

            pharmaco_color = row.pop(0).fill.start_color.index
            drug['category'] = next(category for category in categories if category['color'] == pharmaco_color)['id']

            for (idx, cell) in enumerate(row):
                if fields[idx]['id'] == 'generic_name_trade_name':
                    drug[fields[idx]['id']] = utils._remove_html_tags(cell.value)
                else:
                    drug[fields[idx]['id']] = cell.value

            drugs.append(drug)

        return drugs





