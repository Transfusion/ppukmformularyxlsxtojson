import re


def _format_field_name_underscore(field_name):
    return field_name.replace('(', '').replace(')', '').replace(' ', '_').lower()


def _remove_html_tags(html):
    """To extract tags from the columns in the first row (Generic Name, Formulation, etc)"""
    return re.compile(r'<[^>]+>').sub('', html)
