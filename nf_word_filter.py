from docx import Document


# formulation, indication, dose, admin, notes

class Fields:
    NAME = 'Name'
    COL_PREP = 'Prep.'
    COL_IND = "Ind."
    COL_DOSE = "Dose"
    COL_ADMIN = 'Admin.'
    COL_NOTES = 'Notes'
    COL_NOTE = "Note"


class NFWordFilter:
    def __init__(self, word_path):
        self.word_path = word_path
        self.doc = Document(word_path)

    def _lookup_internal_dict_name(self, field_name):
        if field_name == Fields.COL_NOTES or field_name == Fields.COL_NOTE:
            return 'notes'
        if field_name == Fields.COL_ADMIN:
            return 'admin'
        if field_name == Fields.COL_DOSE:
            return 'dose'
        if field_name == Fields.COL_IND:
            return 'indication'
        if field_name == Fields.COL_PREP:
            return 'formulation'

    def _table_2_json(self, table):
        json_dict = {}
        current_field = Fields.NAME
        for idx, row in enumerate(table.rows):
            cells = list(cell.text for cell in row.cells)
            if idx == 0:
                # if '\n' not in cells[0]:
                #    cells[0] = cells[0] + '\n'

                x = cells[0]

                # cells_split = cells[0].split('\n')
                # generic_name = cells_split[0]
                trade_name = x[x.rfind('(') + 1:x.rfind(')')]
                generic_name = cells[0].replace('(%s)' % trade_name, '')
                generic_name = generic_name.replace('\n', ' ').rstrip()

                json_dict['generic_name'] = generic_name
                json_dict['trade_name'] = trade_name
                continue

            if cells[0] != '':
                current_field = cells[0]
            if current_field == Fields.COL_PREP:
                json_dict[self._lookup_internal_dict_name(current_field)] = cells[1]

            if current_field == Fields.COL_IND:
                json_dict[self._lookup_internal_dict_name(current_field)] = cells[1]

            if current_field == Fields.COL_DOSE:
                json_dict[self._lookup_internal_dict_name(current_field)] = cells[1]

            if current_field == Fields.COL_ADMIN:
                json_dict[self._lookup_internal_dict_name(current_field)] = cells[1]

            if current_field == Fields.COL_NOTES or current_field == Fields.COL_NOTE:
                json_dict[self._lookup_internal_dict_name(current_field)] = cells[1]

            if cells[0] == '':
                z = json_dict[self._lookup_internal_dict_name(current_field)]
                json_dict[self._lookup_internal_dict_name(current_field)] = z + '\n' + cells[1]

        return json_dict

    def get_drugs(self):
        l = []
        for table in self.doc.tables:
            l.append(self._table_2_json(table))
        return l
